FROM docker:stable-dind

RUN apk update && \
    apk add --no-cache \
        python3-dev py3-pip gcc git curl build-base \
        autoconf automake py3-cryptography linux-headers \
        musl-dev libffi-dev openssl-dev openssh

# Ansible version
ARG ANSIBLE_VERSION
ENV ANSIBLE_VERSION=${ANSIBLE_VERSION:-2.10}

# Molecule version
ARG MOLECULE_VERSION
ENV MOLECULE_VERSION=${MOLECULE_VERSION:-3.2}

# Version constraint
ARG VERSION_CONST
ENV VERSION_CONST=${VERSION_CONST:-==}

# Ansible-lint version and constraint
ARG ANSIBLE_LINT_VERSION
ENV ANSIBLE_LINT_VERSION=${ANSIBLE_LINT_VERSION:-}
ARG ANSIBLE_LINT_CONST
ENV ANSIBLE_LINT_CONST=${ANSIBLE_LINT_CONST:-}

# Installing applications
RUN python3 -m pip install \
    "ansible${VERSION_CONST}${ANSIBLE_VERSION}" \
    "molecule[docker]${VERSION_CONST}${MOLECULE_VERSION}" \
    "ansible-lint${ANSIBLE_LINT_CONST}${ANSIBLE_LINT_VERSION}" \
    yamllint \
    flake8