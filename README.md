Ansible & Molecule docker image
===============================

Ansible and Molecule docker image for CI/CD with DockerInDocker (dind) support.

Image based on [docker dind](https://hub.docker.com/_/docker) ([Dockerfile](https://github.com/docker-library/docker/blob/094faa88f437cafef7aeb0cc36e75b59046cc4b9/20.10/dind/Dockerfile)).

## Versions

Compatibility :

| Versions      | ansible 2.8        | ansible 2.9        | ansible 2.10       | ansible 3.0        |
|:------------: | :----------------: | :----------------: | :----------------: | :----------------: |
| molecule 3.2  | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| molecule 3.1  | :white_check_mark: | :white_check_mark: | :white_check_mark: | :grey_question:    |
| molecule 3.0  | :x:                | :x:                | :x:                | :x:                |

### Supported tags

Image registry : [`registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule`](https://gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/container_registry/).

Stable image : `registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/stable:latest`

| Molecule     | Ansible      | Tags                                                                                                                                  |
| :----------- | :----------- | :------------------------------------------------------------------------------------------------------------------------------------ |
| Molecule 3.2 | Ansible 3.0  | `releases/ansible_3.0-molecule_3.2`, `releases/molecule-3.2:ansible-3.0`, `releases/ansible-3.0:molecule-3.2`                         |
| Molecule 3.2 | Ansible 2.10 | `releases/ansible_2.10-molecule_3.2`, `releases/molecule-3.2:ansible-2.10`, `releases/ansible-2.10:molecule-3.2`, **`stable:latest`** |
| Molecule 3.2 | Ansible 2.9  | `releases/ansible_2.9-molecule_3.2`, `releases/molecule-3.2:ansible-2.9`, `releases/ansible-2.9:molecule-3.2`                         |
| Molecule 3.2 | Ansible 2.8  | `releases/ansible_2.8-molecule_3.2`, `releases/molecule-3.2:ansible-2.8`, `releases/ansible-2.8:molecule-3.2`                         |
|              |              |                                                                                                                                       |
| Molecule 3.1 | Ansible 2.10 |	`releases/ansible_2.10-molecule_3.1`, `releases/molecule-3.1:ansible-2.10`, `releases/ansible-2.10:molecule-3.1`                      |
| Molecule 3.1 | Ansible 2.9  | `releases/ansible_2.9-molecule_3.1`, `releases/molecule-3.1:ansible-2.9`, `releases/ansible-2.9:molecule-3.1`                         |
| Molecule 3.1 | Ansible 2.8  | `releases/ansible_2.8-molecule_3.1`, `releases/molecule-3.1:ansible-2.8`, `releases/ansible-2.8:molecule-3.1`                         |

## Usage

### Docker image

You can use the docker image to run specific molecule or ansible version in any env.

Depending on how you start your container you can allow molecule docker provisionner, share your ssh-agent and mount your current directory inside the container. For example :

```bash
docker run \
  --rm --tty --interactive --privileged \
  --volume "$SSH_AUTH_SOCK:/ssh-agent" --env "SSH_AUTH_SOCK=/ssh-agent" \
  --volume "/var/run/docker.sock:/var/run/docker.sock" \
  --volume "$(pwd):$(pwd)" --workdir "$(pwd)" \
  registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/stable \
  ash
```

You can also create various bash alias to run specific version or tools, add in your `~/.bash_aliases` :

```bash
## ansible-molecule docker shell
# molecule-docker-shell : stable:latest - molecule 3.2 ansible 2.10
alias molecule-docker-shell='docker run --rm --tty --interactive --privileged --volume "$SSH_AUTH_SOCK:/ssh-agent" --env "SSH_AUTH_SOCK=/ssh-agent" --volume "/var/run/docker.sock:/var/run/docker.sock" --volume "$(pwd):$(pwd)" --workdir "$(pwd)" registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/stable ash'

## Molecule
# molecule-docker : stable:latest - molecule 3.2 ansible 2.10
alias molecule-docker='docker run --rm --tty --interactive --privileged --volume "$SSH_AUTH_SOCK:/ssh-agent" --env "SSH_AUTH_SOCK=/ssh-agent" --volume "/var/run/docker.sock:/var/run/docker.sock" --volume "$(pwd):$(pwd)" --workdir "$(pwd)" registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/stable molecule'

# molecule-docker-3.1 : molecule 3.1 and ansible 2.10
alias molecule-docker-3.1='docker run --rm --tty --interactive --privileged --volume "$SSH_AUTH_SOCK:/ssh-agent" --env "SSH_AUTH_SOCK=/ssh-agent" --volume "/var/run/docker.sock:/var/run/docker.sock" --volume "$(pwd):$(pwd)" --workdir "$(pwd)" registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/releases/molecule-3.1:ansible-2.10 molecule'

## Ansible
# ansible-docker : stable:latest - ansible 2.10 molecule 3.2
alias ansible-docker='docker run --rm --tty --interactive --privileged --volume "$SSH_AUTH_SOCK:/ssh-agent" --env "SSH_AUTH_SOCK=/ssh-agent" --volume "/var/run/docker.sock:/var/run/docker.sock" --volume "$(pwd):$(pwd)" --workdir "$(pwd)" registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/stable ansible'

# ansible-docker-3.0 : ansible 3.0 and molecule 3.2
alias ansible-docker-3.0='docker run --rm --tty --interactive --privileged --volume "$SSH_AUTH_SOCK:/ssh-agent" --env "SSH_AUTH_SOCK=/ssh-agent" --volume "/var/run/docker.sock:/var/run/docker.sock" --volume "$(pwd):$(pwd)" --workdir "$(pwd)" registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/releases/ansible-3.0:molecule-3.2 ansible'
```

Then you can run molecule via one of the aliases :

```bash
$ molecule-docker --version
molecule 3.2.0 using python 3.8
    ansible:2.10.6
    delegated:3.2.0 from molecule
    docker:0.2.4 from molecule_docker
```

### CI/CD

In `.gitlab-ci.yml`, load image and run molecule or ansible commands.

#### Example molecule

```yaml
# Load molecule and ansible image.
# Tags on : https://gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/-/releases.
image: registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/stable:latest

stages:
  - test
  - release

services:
  - docker:dind

include:
  - project: 'botux-fr/ci-cd/gitlab-ci-templates'
    # Change ref to release tag from https://gitlab.com/botux-fr/ci-cd/gitlab-ci-templates/-/releases.
    ref: main
    file: '/release/semantic-release.yml'

before_script:
  - ansible --version
  - molecule --version

variables:
  SCENARIO: default
  PY_COLORS: 1

test:default:
  stage: test
  script:
    - molecule test -s $SCENARIO

release dryRun:
  extends: .semantic-release
  variables:
    EXTRA_ARGS: "-d"
  rules:
    - if: '$CI_COMMIT_BRANCH == "CI_DEFAULT_BRANCH" || $CI_COMMIT_BRANCH == "main"'
      when: never
    - if: '$CI_COMMIT_BRANCH'

release publish:
  extends: .semantic-release
  # The publish job depends on the test job before releasing the new version.
  dependencies:
    - test:default
  rules:
    - if: '$CI_COMMIT_BRANCH == "CI_DEFAULT_BRANCH" || $CI_COMMIT_BRANCH == "main"'
```

#### Example ansible

You can use the same image to run ansible-playbook or other ansible-* commands from your ci.

```yaml
# Load molecule and ansible image.
# Tags on : https://gitlab.com/botux-fr/ci-cd/docker/ansible-molecule/-/releases.
image: registry.gitlab.com/botux-fr/ci-cd/docker/ansible-molecule:stable:latest

include:
  - project: 'botux-fr/ci-cd/gitlab-ci-templates'
    # Change ref to release tag from https://gitlab.com/botux-fr/ci-cd/gitlab-ci-templates/-/releases.
    ref: main
    file: '/release/semantic-release.yml'

stages:
  - test
  - check
  - dryrun
  - run
  - release

services:
  - docker:dind

variables:
  PY_COLORS: 1

before_script:
  - 'which ssh-agent || ( apk add --update openssh-client git -y )'
  - eval $(ssh-agent -s)
  # Load SSH_PRIVATE_KEY from env vars.
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  # Optionnal load SSH_PRIVATE_KEY_STAGING from env vars.
  - echo "$SSH_PRIVATE_KEY_STAGING" | tr -d '\r' | ssh-add - > /dev/null || true
  - mkdir -p ~/.ssh && chmod 700 ~/.ssh
  - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  - echo -e "machine gitlab.com\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > ~/.netrc && chmod 600 ~/.netrc
  - ansible --version
  - ansible-galaxy install -r requirements.yml

lint:
  stage: test
  script:
    - ansible-lint playbooks/*

pre ping:
  stage: check
  script:
    - ansible-inventory -i inventory/hosts.yaml --graph --vars
    - ANSIBLE_HOST_KEY_CHECKING=false ansible -i inventory/hosts.yaml -m ping all --become --user ubuntu

pre configuration:
  stage: check
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/config.yaml --user ubuntu --list-hosts
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/config.yaml --user ubuntu --list-tasks

pre init:
  stage: check
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/init.yaml --user ubuntu --list-hosts
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/init.yaml --user ubuntu --list-tasks

dryrun configuration:
  stage: dryrun
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/config.yaml --diff --user ubuntu --check
  allow_failure: true

dryrun init:
  stage: dryrun
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/init.yaml --diff --user ubuntu --check
  allow_failure: true

run staging configuration:
  stage: run
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/config.yaml --diff --user ubuntu -l staging
  when: manual
  only:
    - branches
    - staging
    - web

run staging init:
  stage: run
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/init.yaml --diff --user ubuntu -l staging
  when: manual
  only:
    - staging
    - web

run production configuration:
  stage: run
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/config.yaml --diff --user ubuntu -l production
  when: manual
  only:
    - tags
    - main
    - triggers
    - schedules
    - web

run production init:
  stage: run
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/init.yaml --diff --user ubuntu -l production
  when: manual
  only:
    - tags
    - main
    - triggers
    - schedules
    - web

run all configuration:
  stage: run
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/config.yaml --diff --user ubuntu
  when: manual
  only:
    - tags
    - main
    - triggers
    - schedules
    - web

run all init:
  stage: run
  resource_group: ansible-project-example-1
  script:
    - ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i inventory/hosts.yaml playbooks/init.yaml --diff --user ubuntu
  when: manual
  only:
    - tags
    - main
    - triggers
    - schedules
    - web

release dryRun:
  extends: .semantic-release
  variables:
    EXTRA_ARGS: "-d"
  rules:
    - if: '$CI_COMMIT_BRANCH == "CI_DEFAULT_BRANCH" || $CI_COMMIT_BRANCH == "main"'
      when: never
    - if: '$CI_COMMIT_BRANCH'

release publish:
  extends: .semantic-release
  # The publish job depends on the test job before releasing the new version.
  dependencies:
    - test:default
  rules:
    - if: '$CI_COMMIT_BRANCH == "CI_DEFAULT_BRANCH" || $CI_COMMIT_BRANCH == "main"'
```

## Authors

* [Romain Fluttaz](https://gitlab.com/botux/)